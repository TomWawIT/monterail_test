import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Questions from '@/components/Questions'
import Question from '@/components/Question'

Vue.use(Router)

export default new Router({
	mode: 'history',
	routes: [
		{
			path: '/',
			name: 'Home',
			component: Home
		},
		{
			path: '/questions',
			name: 'Questions',
			component: Questions
		},
		{
			path: '/question',
			name: 'Question',
			component: Question
		}
	]
})
